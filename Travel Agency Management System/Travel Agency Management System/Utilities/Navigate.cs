﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Travel_Agency_Management_System.Forms.Admin;
using Travel_Agency_Management_System.Forms.Customer;

namespace Travel_Agency_Management_System.Utilities
{
    public class Navigate
    {
        public static void ToHotelRooms()
        {
            if (MainForm.Instance.pnlContainer.Controls.Contains(ucRooms.Instance))
            {
                ucRooms.Instance.RefreshUI();
                ucRooms.Instance.BringToFront();
            }
            else
            {
                MainForm.Instance.pnlContainer.Controls.Add(ucRooms.Instance);
                ucRooms.Instance.Dock = DockStyle.Fill;
                ucRooms.Instance.BringToFront();
            }

        }
        public static void ToHotels()
        {
            if (MainForm.Instance.pnlContainer.Controls.Contains(ucHotels.Instance))
            {
                //ucHotels.Instance.RefreshUI();
                ucHotels.Instance.BringToFront();
            }
            else
            {
                MainForm.Instance.pnlContainer.Controls.Add(ucHotels.Instance);
                ucHotels.Instance.Dock = DockStyle.Fill;
                ucHotels.Instance.BringToFront();
            }

        }
        public static void ToTrips()
        {
            if (MainForm.Instance.pnlContainer.Controls.Contains(ucTrips.Instance))
            {
                ucTrips.Instance.RefreshUI();
                ucTrips.Instance.BringToFront();
            }
            else
            {
                MainForm.Instance.pnlContainer.Controls.Add(ucTrips.Instance);
                ucTrips.Instance.Dock = DockStyle.Fill;
                ucTrips.Instance.BringToFront();
            }
        }
        public static void ToVehicles()
        {
            if (MainForm.Instance.pnlContainer.Controls.Contains(ucVehicles.Instance))
            {
                ucVehicles.Instance.RefreshUI();
                ucVehicles.Instance.BringToFront();
            }
            else
            {
                MainForm.Instance.pnlContainer.Controls.Add(ucVehicles.Instance);
                ucVehicles.Instance.Dock = DockStyle.Fill;
                ucVehicles.Instance.BringToFront();
            }

        }
        public static void ToPayments()
        {
            if (MainForm.Instance.pnlContainer.Controls.Contains(ucPayments.Instance))
            {
                ucPayments.Instance.RefreshUI();
                ucPayments.Instance.BringToFront();
            }
            else
            {
                MainForm.Instance.pnlContainer.Controls.Add(ucPayments.Instance);
                ucPayments.Instance.Dock = DockStyle.Fill;
                ucPayments.Instance.BringToFront();
            }

        }
        public static void ToReports()
        {
            if (MainForm.Instance.pnlContainer.Controls.Contains(ucReports.Instance))
            {
                ucReports.Instance.RefreshUI();
                ucReports.Instance.BringToFront();
            }
            else
            {
                MainForm.Instance.pnlContainer.Controls.Add(ucReports.Instance);
                ucReports.Instance.Dock = DockStyle.Fill;
                ucReports.Instance.BringToFront();
            }
        }
        public static void ToViewHotelPackages()
        {
            if (MainForm.Instance.pnlContainer.Controls.Contains(View_of_Hotels_Packages.Instance))
            {
                //View_of_Hotels_Packages.Instance.RefreshUI();
                View_of_Hotels_Packages.Instance.BringToFront();
            }
            else
            {
                MainForm.Instance.pnlContainer.Controls.Add(View_of_Hotels_Packages.Instance);
                View_of_Hotels_Packages.Instance.Dock = DockStyle.Fill;
                View_of_Hotels_Packages.Instance.BringToFront();
            }
        }
        public static void ToViewHotelROOMs()
        {
            if (MainForm.Instance.pnlContainer.Controls.Contains(View_of_Hotel_and_Hotel_Rooms.Instance))
            {
                //View_of_Hotels_Packages.Instance.RefreshUI();
                View_of_Hotel_and_Hotel_Rooms.Instance.BringToFront();
            }
            else
            {
                MainForm.Instance.pnlContainer.Controls.Add(View_of_Hotel_and_Hotel_Rooms.Instance);
                View_of_Hotel_and_Hotel_Rooms.Instance.Dock = DockStyle.Fill;
                View_of_Hotel_and_Hotel_Rooms.Instance.BringToFront();
            }
        }
        public static void ToBookings()
        {
            if (CustomerMain.Instance.pnlContainer.Controls.Contains(ucBookings.Instance))
            {
                ucBookings.Instance.RefreshUI();
                ucBookings.Instance.BringToFront();
            }
            else
            {
                CustomerMain.Instance.pnlContainer.Controls.Add(ucBookings.Instance);
                ucBookings.Instance.Dock = DockStyle.Fill;
                ucBookings.Instance.BringToFront();
            }
        }
        public static void ToCustomerPayments()
        {
            if (CustomerMain.Instance.pnlContainer.Controls.Contains(ucCustomerPayments.Instance))
            {
                ucCustomerPayments.Instance.RefreshUI();
                ucCustomerPayments.Instance.BringToFront();
            }
            else
            {
                CustomerMain.Instance.pnlContainer.Controls.Add(ucCustomerPayments.Instance);
                ucCustomerPayments.Instance.Dock = DockStyle.Fill;
                
                ucCustomerPayments.Instance.BringToFront();
            }
        }
        public static void ToDashboard()
        {
            if (CustomerMain.Instance.pnlContainer.Controls.Contains(ucDashboard.Instance))
            {
                ucDashboard.Instance.RefreshUI();
                ucDashboard.Instance.BringToFront();
            }
            else
            {
                CustomerMain.Instance.pnlContainer.Controls.Add(ucDashboard.Instance);
                ucDashboard.Instance.Dock = DockStyle.Fill;

                ucDashboard.Instance.BringToFront();
            }
        }

        public static void ToMyTickets()
        {
            if (CustomerMain.Instance.pnlContainer.Controls.Contains(ucMyTickets.Instance))
            {
                ucMyTickets.Instance.RefreshUI();
                ucMyTickets.Instance.BringToFront();
            }
            else
            {
                CustomerMain.Instance.pnlContainer.Controls.Add(ucMyTickets.Instance);
                ucMyTickets.Instance.Dock = DockStyle.Fill;
                ucMyTickets.Instance.BringToFront();
            }
        }


        public static void ToReview()
        {
            if (CustomerMain.Instance.pnlContainer.Controls.Contains(ucReview.Instance))
            {
                ucReview.Instance.RefreshUI();
                ucReview.Instance.BringToFront();
            }
            else
            {
                CustomerMain.Instance.pnlContainer.Controls.Add(ucReview.Instance);
                ucReview.Instance.Dock = DockStyle.Fill;
                ucReview.Instance.BringToFront();
            }
        }

        public static void ToBookingTicket1()
        {
            if (CustomerMain.Instance.pnlContainer.Controls.Contains(ucBookingTicket1.Instance))
            {
                //ucBookingTicket1.Instance.RefreshUI();
                ucBookingTicket1.Instance.BringToFront();
            }
            else
            {
                CustomerMain.Instance.pnlContainer.Controls.Add(ucBookingTicket1.Instance);
                ucBookingTicket1.Instance.Dock = DockStyle.Fill;

                ucBookingTicket1.Instance.BringToFront();
            }
        }

    }
}
