﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Agency_Management_System.BL.AdminSection
{
    public class Admin_VehiclesClass
    {
        private string vehicle_type;
        private string vehicle_model;
        private int no_of_seats;
        private int price_per_seat;

        public Admin_VehiclesClass(string vehicle_type, string vehicle_model, int no_of_seats, int price_per_seat)
        {
            this.vehicle_type = vehicle_type;
            this.vehicle_model = vehicle_model;
            this.no_of_seats = no_of_seats;
            this.price_per_seat = price_per_seat;
        }

        public string Vehicle_type { get => vehicle_type; set => vehicle_type = value; }
        public string Vehicle_model{ get => vehicle_model; set => vehicle_model = value; }
        public int No_of_seats { get => no_of_seats; set => no_of_seats = value; }
        public int Price_per_seat { get => price_per_seat; set => price_per_seat = value; }
    }
}
