﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Agency_Management_System.BL.AdminSection
{
    public class Admin_HotelClass
    {

      
        private string hotelName;
        private int price;
        private string location;
        private string review;

        public Admin_HotelClass(string hotelName , int price , string location , string review)
        {
            
            this.Location = location;
            this.Price = price;
            this.HotelName = hotelName;
            this.Review = review;

        }

        public string HotelName { get => hotelName; set => hotelName = value; }
        public int Price { get => price; set => price = value; }
        public string Location { get => location; set => location = value; }
        public string Review { get => review; set => review = value; }
       
    }
}
