﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Agency_Management_System.BL.AdminSection
{
    public class Admin_PaymentsClass
    {
        private string paymenent_method;

        public Admin_PaymentsClass(string method)
        {
            this.Paymenent_method = method;
        }

        public string Paymenent_method { get => paymenent_method; set => paymenent_method = value; }
    }
}
