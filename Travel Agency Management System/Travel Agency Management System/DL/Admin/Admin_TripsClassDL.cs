﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travel_Agency_Management_System.BL.AdminSection;

namespace Travel_Agency_Management_System.DL.Admin
{
    public class Admin_TripsClassDL
    {
        private static List<Admin_TripsClass> packageslist = new List<Admin_TripsClass>();
        public static List<Admin_TripsClass> Packageslist { get => packageslist; set => packageslist = value; }

        public static void load_Packages_from_DataBase()
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Select * from Packages", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);



            foreach (DataRow row in dt.Rows)
            {

                string Name= row["PackageName"].ToString();
                int price = (int)row["PackagePrice"];
                string departure = row["Departure"].ToString();
                int noofdays = (int)row["NoofDays"];
                string destination = row["Destination"].ToString();
                Image pic = (Image)row["Picture"];

                Admin_TripsClass package = new Admin_TripsClass(p_name: Name, price, departure, noofdays, destination, pic);
                Admin_TripsClassDL.Packageslist.Add(package);

            }

        }
    }
}
