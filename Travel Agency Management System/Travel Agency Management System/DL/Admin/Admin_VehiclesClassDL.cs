﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Travel_Agency_Management_System.BL.AdminSection;

namespace Travel_Agency_Management_System.DL.Admin
{
    public class Admin_VehiclesClassDL
    {
        private static List<Admin_VehiclesClass> VehicleClassList = new List<Admin_VehiclesClass>();

        public static List<Admin_VehiclesClass> VehicleClassList1 { get => VehicleClassList; set => VehicleClassList = value; }

        //        public static List<Admin_VehiclesClass> HotelClassList1 { get => HotelClassList; set => HotelClassList = value; }

        //        public static List<Admin_HotelClass> HotelClassList2 { get => HotelClassList; set => HotelClassList = value; }


        public static void Add_vehicle_data_in_List(Admin_VehiclesClass vehicles)
        {
            // Check if the hotel already exists in the list
            bool VechicleExists = false;
            foreach (Admin_VehiclesClass existingVechicle in VehicleClassList1)
            {
                if (existingVechicle.Vehicle_type == vehicles.Vehicle_type && existingVechicle.No_of_seats == vehicles.No_of_seats && existingVechicle.Price_per_seat == vehicles.Price_per_seat && existingVechicle.Vehicle_model == vehicles.Vehicle_model)
                {
                    // The hotel already exists, set the flag to true and break out of the loop
                    VechicleExists = true;
                    break;
                }
            }
            if (!VechicleExists)
            {
                VehicleClassList1.Add(vehicles);
                MessageBox.Show("Record Entered Successfully !");
            }
            else
            {
                MessageBox.Show("Vechicle already exists in the list.");
            }
        }
        public static void delete_vehicle_from_list(Admin_VehiclesClass vehicle)
        {
            for (int i = VehicleClassList1.Count - 1; i >= 0; i--)
            {
                Admin_VehiclesClass n = VehicleClassList1[i];
                if (n.Vehicle_type == vehicle.Vehicle_type && n.No_of_seats == vehicle.No_of_seats && n.Price_per_seat == vehicle.Price_per_seat && n.Vehicle_model == vehicle.Vehicle_model)
                {
                    VehicleClassList1.RemoveAt(i);
                }
            }

        }

        public static bool checkitsexist(Admin_VehiclesClass vehicle, Admin_VehiclesClass vehicle1)
        {
            foreach (Admin_VehiclesClass existingVehicle in VehicleClassList1)
            {
                if (existingVehicle.Vehicle_type == vehicle1.Vehicle_type && existingVehicle.No_of_seats == vehicle1.No_of_seats  && existingVehicle.Price_per_seat == vehicle1.Price_per_seat && existingVehicle.Vehicle_model == vehicle1.Vehicle_model)
                {


                    existingVehicle.Vehicle_model = vehicle.Vehicle_model;
                    existingVehicle.Price_per_seat = vehicle.Price_per_seat;
                    existingVehicle.Vehicle_model = vehicle.Vehicle_model;
                    existingVehicle.No_of_seats = vehicle.No_of_seats;
                    return true;

                }
            }
            return false;

        }
        public static void AddVechilesToDatabase(Admin_VehiclesClass vehicles)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_AddVehicle", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@vehicle_type", vehicles.Vehicle_type);
            cmd.Parameters.AddWithValue("@price_per_seat", vehicles.Price_per_seat);
            cmd.Parameters.AddWithValue("@model", vehicles.Vehicle_model);
            cmd.Parameters.AddWithValue("@total_seats", vehicles.No_of_seats);
            cmd.ExecuteNonQuery();


        }

        public static void load_VEHICLE_data_from_DataBase()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_LoadVehicleData", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                string type = row["vehicle_type"].ToString();
                int Price = (int)row["price_per_seat"];
                string model = row["model"].ToString();
                int total_seats = (int)row["total_seats"];
                Admin_VehiclesClass vehicles = new Admin_VehiclesClass(type, model, total_seats, Price);
                Admin_VehiclesClassDL.VehicleClassList1.Add(vehicles);
            }

        }
    }
}
