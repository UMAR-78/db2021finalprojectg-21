﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Travel_Agency_Management_System.BL.AdminSection;
using Travel_Agency_Management_System.Forms.Admin;
using Travel_Agency_Management_System.Forms;
using System.Data;

namespace Travel_Agency_Management_System.DL.Admin
{
    public class Admin_HotelClassDL

    {

        private static List<Admin_HotelClass> HotelClassList = new List<Admin_HotelClass>();

        public static List<Admin_HotelClass> HotelClassList2 { get => HotelClassList; set => HotelClassList = value; }


        public static void Add_hotel_data_in_List(Admin_HotelClass hotel)
        {
            // Check if the hotel already exists in the list
            bool hotelExists = false;
            foreach (Admin_HotelClass existingHotel in HotelClassList)
            {
                if (existingHotel.HotelName == hotel.HotelName && existingHotel.Location == hotel.Location && existingHotel.Review == hotel.Review && existingHotel.Price == hotel.Price)
                {
                    // The hotel already exists, set the flag to true and break out of the loop
                    hotelExists = true;
                    break;
                }
            }
            if (!hotelExists)
            {
                HotelClassList2.Add(hotel);
                MessageBox.Show("Record Entered Successfully !");
            }
            else
            {
                MessageBox.Show("Hotel already exists in the list.");
            }
        }
        public static void delete_hotel_from_list(Admin_HotelClass hotel)
        {
            for (int i = HotelClassList.Count - 1; i >= 0; i--)
            {
                Admin_HotelClass n = HotelClassList[i];
                if (n.HotelName == hotel.HotelName && n.Price == hotel.Price && n.Location == hotel.Location && n.Review == hotel.Review)
                {
                    HotelClassList.RemoveAt(i);
                }
            }

        }

        public static bool checkitsexist(Admin_HotelClass hotel, Admin_HotelClass hotel1)
        {
            foreach (Admin_HotelClass existingHotel in HotelClassList)
            {
                if (existingHotel.HotelName == hotel1.HotelName && existingHotel.Location == hotel1.Location && existingHotel.Review == hotel1.Review && existingHotel.Price == hotel1.Price)
                {


                    existingHotel.HotelName = hotel.HotelName;
                    existingHotel.Price = hotel.Price;
                    existingHotel.Location = hotel.Location;
                    existingHotel.Review = hotel.Review;
                    return true;

                }
            }
            return false;

        }

        /* public static void updatehotelifexist(Admin_HotelClass existingHotel, Admin_HotelClass h)
         {
             foreach (Admin_HotelClass existingHotel in HotelClassList)
             {
                 if (existingHotel.HotelName == hotel.HotelName && existingHotel.Location == hotel.Location && existingHotel.Review == hotel.Review && existingHotel.Price == hotel.Price)
                 {
                     existingHotel.HotelName = hotel.HotelName;
                     existingHotel.Price = hotel.Price;
                     existingHotel.Location = hotel.Location;
                     existingHotel.Review = hotel.Review;
                 }
             }
         }*/
        
        
        public static void AddHotelToDatabase(Admin_HotelClass hotel)
        {
            var con = Configuration.getInstance().getConnection();
            using (SqlCommand cmd = new SqlCommand("AddHotel", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@HotelName", hotel.HotelName);
                cmd.Parameters.AddWithValue("@Price", hotel.Price);
                cmd.Parameters.AddWithValue("@Location", hotel.Location);
                cmd.Parameters.AddWithValue("@Review", hotel.Review);
                cmd.ExecuteNonQuery();
            }
        }

        public static void load_data_from_DataBase()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_GetHotels", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                string HotelName = row["Name"].ToString();
                int Price = (int)row["Price"];
                string Location = row["Location"].ToString();
                string Review = row["Review"].ToString();
                Admin_HotelClass hotel = new Admin_HotelClass(HotelName, Price, Location, Review);
                Admin_HotelClassDL.HotelClassList2.Add(hotel);
            }
        }

    }

}