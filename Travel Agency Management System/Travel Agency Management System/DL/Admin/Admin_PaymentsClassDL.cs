﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Travel_Agency_Management_System.BL.AdminSection;

namespace Travel_Agency_Management_System.DL.Admin
{
    public class Admin_PaymentsClassDL
    {
        private static List<Admin_PaymentsClass> PaymentsClassList = new List<Admin_PaymentsClass>();

        public static List<Admin_PaymentsClass> PaymentsClassList1 { get => PaymentsClassList; set => PaymentsClassList = value; }

        // public static List<Admin_PaymentsClass> PaymentsClassList2 { get => PaymentsClassList; set => PaymentsClassList = value; }


        public static void Add_payment_data_in_List(Admin_PaymentsClass payments)
        {
            // Check if the hotel already exists in the list
            bool PaymentsExists = false;
            foreach (Admin_PaymentsClass existingPayment in PaymentsClassList1)
            {
                if (existingPayment.Paymenent_method == payments.Paymenent_method )
                {
                    // The hotel already exists, set the flag to true and break out of the loop
                    PaymentsExists = true;
                    break;
                }
            }
            if (!PaymentsExists)
            {
                PaymentsClassList1.Add(payments);
                MessageBox.Show("Record Entered Successfully !");
            }
            else
            {
                MessageBox.Show("Payment method already exists in the list.");
            }
        }
        public static void delete_payment_from_list(Admin_PaymentsClass payments)
        {
            for (int i = PaymentsClassList1.Count - 1; i >= 0; i--)
            {
                Admin_PaymentsClass n = PaymentsClassList1[i];
                if (n.Paymenent_method == payments.Paymenent_method )
                {
                    PaymentsClassList1.RemoveAt(i);
                }
            }

        }

        public static bool checkitsexist(Admin_PaymentsClass Payments, Admin_PaymentsClass payments1)
        {
            foreach (Admin_PaymentsClass existingPayment in PaymentsClassList1)
            {
                if (existingPayment.Paymenent_method == payments1.Paymenent_method )
                {


                    existingPayment.Paymenent_method = Payments.Paymenent_method;
                    return true;

                }
            }
            return false;

        }

       

        public static void AddPaymentsToDatabase(Admin_PaymentsClass Payments)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_AddPaymentsToDatabase", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Methods", Payments.Paymenent_method);
            cmd.ExecuteNonQuery();

        }

        public static void load_Payments_data_from_DataBase()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_load_Payments_data_from_DataBase", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                string methodName = row["Methods"].ToString();
                Admin_PaymentsClass payments = new Admin_PaymentsClass(methodName);
                Admin_PaymentsClassDL.PaymentsClassList1.Add(payments);
            }


        }
    }
}
