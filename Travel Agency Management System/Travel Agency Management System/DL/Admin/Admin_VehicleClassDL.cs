﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travel_Agency_Management_System.BL;
using Travel_Agency_Management_System.BL.AdminSection;

namespace Travel_Agency_Management_System.DL.Admin
{
    public class Admin_VehicleClassDL
    {
        private List<Admin_VehiclesClass> VehicleList = new List<Admin_VehiclesClass>();

        public List<Admin_VehiclesClass> VehicleList1 { get => VehicleList; set => VehicleList = value; }

        public void Addvehiclesintolist(Admin_VehiclesClass n)
        {
            VehicleList1.Add(n);
        }
    }
}
