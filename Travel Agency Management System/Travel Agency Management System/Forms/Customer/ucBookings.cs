﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Travel_Agency_Management_System.Forms.Customer
{
    public partial class ucBookings : UserControl
    {
        private static ucBookings _instance;
        public static ucBookings Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucBookings();
                }
                return _instance;
            }
        }

        public void RefreshUI()
        {

        }
        public ucBookings()
        {
            InitializeComponent();
        }
    }
}
