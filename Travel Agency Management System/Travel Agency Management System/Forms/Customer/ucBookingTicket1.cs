﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Travel_Agency_Management_System.BL.AdminSection;

namespace Travel_Agency_Management_System.Forms.Customer
{
    public partial class ucBookingTicket1 : UserControl
    {
        private Admin_TripsClass packageSelected;

        private static ucBookingTicket1 _instance;
        public static ucBookingTicket1 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucBookingTicket1();
                }
                return _instance;

            }
        }

        private ucBookingTicket1()
        {
            InitializeComponent();
        }

        public void LoadPackage(Admin_TripsClass obj)
        {
            lbPackageName.Text = obj.PackageName;
            pictureBox1.Image = obj.Pic;
            packageSelected = obj;
        }
    }
}
