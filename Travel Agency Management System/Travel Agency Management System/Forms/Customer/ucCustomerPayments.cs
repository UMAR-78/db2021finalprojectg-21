﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Travel_Agency_Management_System.Forms.Customer
{
    public partial class ucCustomerPayments : UserControl
    {
        private static ucCustomerPayments _instance;
        public static ucCustomerPayments Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucCustomerPayments();
                }
                return _instance;
            }
        }

        public void RefreshUI()
        {

        }
        public ucCustomerPayments()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
