﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Travel_Agency_Management_System.Forms.Customer
{
    public partial class ucMyUserControl : UserControl
    {
        public ucMyUserControl()
        {
            InitializeComponent();
        }

        public Bitmap Icon { get; internal set; }
    }
}
