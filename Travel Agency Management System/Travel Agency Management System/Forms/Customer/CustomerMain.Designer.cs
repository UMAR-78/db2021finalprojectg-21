﻿
namespace Travel_Agency_Management_System.Forms.Customer
{
    partial class CustomerMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomerMain));
            this.pnlContainer = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbtn_Exit = new Travel_Agency_Management_System.Properties.RoundButton();
            this.rbtn_Review = new Travel_Agency_Management_System.Properties.RoundButton();
            this.rbtn_Payments = new Travel_Agency_Management_System.Properties.RoundButton();
            this.rbtn_Bookings = new Travel_Agency_Management_System.Properties.RoundButton();
            this.rbtn_MyTickets = new Travel_Agency_Management_System.Properties.RoundButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.rbtnDashboard = new Travel_Agency_Management_System.Properties.RoundButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlContainer
            // 
            this.pnlContainer.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlContainer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlContainer.BackgroundImage")));
            this.pnlContainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContainer.Location = new System.Drawing.Point(175, 0);
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new System.Drawing.Size(730, 487);
            this.pnlContainer.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.rbtn_Exit);
            this.panel1.Controls.Add(this.rbtn_Review);
            this.panel1.Controls.Add(this.rbtn_Payments);
            this.panel1.Controls.Add(this.rbtn_Bookings);
            this.panel1.Controls.Add(this.rbtn_MyTickets);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.rbtnDashboard);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(175, 487);
            this.panel1.TabIndex = 2;
            // 
            // rbtn_Exit
            // 
            this.rbtn_Exit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.rbtn_Exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.rbtn_Exit.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.rbtn_Exit.BorderColor = System.Drawing.Color.Transparent;
            this.rbtn_Exit.BorderRadius = 7;
            this.rbtn_Exit.BorderSize = 0;
            this.rbtn_Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtn_Exit.FlatAppearance.BorderSize = 0;
            this.rbtn_Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtn_Exit.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_Exit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Exit.Location = new System.Drawing.Point(11, 441);
            this.rbtn_Exit.Name = "rbtn_Exit";
            this.rbtn_Exit.Size = new System.Drawing.Size(147, 30);
            this.rbtn_Exit.TabIndex = 35;
            this.rbtn_Exit.Text = "Exit";
            this.rbtn_Exit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtn_Exit.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Exit.UseVisualStyleBackColor = false;
            this.rbtn_Exit.Click += new System.EventHandler(this.rbtn_Exit_Click);
            this.rbtn_Exit.MouseEnter += new System.EventHandler(this.rbtn_Exit_MouseEnter);
            this.rbtn_Exit.MouseLeave += new System.EventHandler(this.rbtn_Exit_MouseLeave);
            // 
            // rbtn_Review
            // 
            this.rbtn_Review.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rbtn_Review.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.rbtn_Review.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.rbtn_Review.BorderColor = System.Drawing.Color.Transparent;
            this.rbtn_Review.BorderRadius = 7;
            this.rbtn_Review.BorderSize = 0;
            this.rbtn_Review.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtn_Review.FlatAppearance.BorderSize = 0;
            this.rbtn_Review.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtn_Review.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_Review.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Review.Location = new System.Drawing.Point(11, 333);
            this.rbtn_Review.Name = "rbtn_Review";
            this.rbtn_Review.Size = new System.Drawing.Size(147, 30);
            this.rbtn_Review.TabIndex = 33;
            this.rbtn_Review.Text = "Review";
            this.rbtn_Review.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtn_Review.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Review.UseVisualStyleBackColor = false;
            this.rbtn_Review.Click += new System.EventHandler(this.rbtn_Review_Click);
            this.rbtn_Review.MouseEnter += new System.EventHandler(this.rbtn_Review_MouseEnter);
            this.rbtn_Review.MouseLeave += new System.EventHandler(this.rbtn_Review_MouseLeave);
            // 
            // rbtn_Payments
            // 
            this.rbtn_Payments.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rbtn_Payments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.rbtn_Payments.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.rbtn_Payments.BorderColor = System.Drawing.Color.Transparent;
            this.rbtn_Payments.BorderRadius = 7;
            this.rbtn_Payments.BorderSize = 0;
            this.rbtn_Payments.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtn_Payments.FlatAppearance.BorderSize = 0;
            this.rbtn_Payments.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtn_Payments.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_Payments.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Payments.Location = new System.Drawing.Point(12, 293);
            this.rbtn_Payments.Name = "rbtn_Payments";
            this.rbtn_Payments.Size = new System.Drawing.Size(147, 30);
            this.rbtn_Payments.TabIndex = 32;
            this.rbtn_Payments.Text = "Payments";
            this.rbtn_Payments.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtn_Payments.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Payments.UseVisualStyleBackColor = false;
            this.rbtn_Payments.Click += new System.EventHandler(this.rbtn_Payments_Click);
            this.rbtn_Payments.MouseEnter += new System.EventHandler(this.rbtn_Payments_MouseEnter);
            this.rbtn_Payments.MouseLeave += new System.EventHandler(this.rbtn_Payments_MouseLeave);
            // 
            // rbtn_Bookings
            // 
            this.rbtn_Bookings.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rbtn_Bookings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.rbtn_Bookings.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.rbtn_Bookings.BorderColor = System.Drawing.Color.Transparent;
            this.rbtn_Bookings.BorderRadius = 7;
            this.rbtn_Bookings.BorderSize = 0;
            this.rbtn_Bookings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtn_Bookings.FlatAppearance.BorderSize = 0;
            this.rbtn_Bookings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtn_Bookings.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_Bookings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Bookings.Location = new System.Drawing.Point(12, 253);
            this.rbtn_Bookings.Name = "rbtn_Bookings";
            this.rbtn_Bookings.Size = new System.Drawing.Size(147, 30);
            this.rbtn_Bookings.TabIndex = 31;
            this.rbtn_Bookings.Text = "Bookings";
            this.rbtn_Bookings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtn_Bookings.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Bookings.UseVisualStyleBackColor = false;
            this.rbtn_Bookings.Click += new System.EventHandler(this.rbtn_Bookings_Click);
            this.rbtn_Bookings.MouseEnter += new System.EventHandler(this.rbtn_Bookings_MouseEnter);
            this.rbtn_Bookings.MouseLeave += new System.EventHandler(this.rbtn_Bookings_MouseLeave);
            // 
            // rbtn_MyTickets
            // 
            this.rbtn_MyTickets.AccessibleName = "";
            this.rbtn_MyTickets.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rbtn_MyTickets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.rbtn_MyTickets.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.rbtn_MyTickets.BorderColor = System.Drawing.Color.Transparent;
            this.rbtn_MyTickets.BorderRadius = 7;
            this.rbtn_MyTickets.BorderSize = 0;
            this.rbtn_MyTickets.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtn_MyTickets.FlatAppearance.BorderSize = 0;
            this.rbtn_MyTickets.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtn_MyTickets.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_MyTickets.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_MyTickets.Location = new System.Drawing.Point(11, 213);
            this.rbtn_MyTickets.Name = "rbtn_MyTickets";
            this.rbtn_MyTickets.Size = new System.Drawing.Size(147, 30);
            this.rbtn_MyTickets.TabIndex = 0;
            this.rbtn_MyTickets.Text = "My Tickets";
            this.rbtn_MyTickets.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtn_MyTickets.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_MyTickets.UseVisualStyleBackColor = false;
            this.rbtn_MyTickets.Click += new System.EventHandler(this.rbtn_MyTickets_Click);
            this.rbtn_MyTickets.MouseEnter += new System.EventHandler(this.rbtn_MyTickets_MouseEnter);
            this.rbtn_MyTickets.MouseLeave += new System.EventHandler(this.rbtn_MyTickets_MouseLeave);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(171, 167);
            this.panel2.TabIndex = 30;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.label2.Location = new System.Drawing.Point(16, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 17);
            this.label2.TabIndex = 29;
            this.label2.Text = "Customer Dashboard";
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel3.Location = new System.Drawing.Point(57, 29);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(58, 47);
            this.panel3.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.label3.Location = new System.Drawing.Point(13, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 30);
            this.label3.TabIndex = 18;
            this.label3.Text = "We Travelers";
            // 
            // rbtnDashboard
            // 
            this.rbtnDashboard.AccessibleName = "";
            this.rbtnDashboard.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rbtnDashboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.rbtnDashboard.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.rbtnDashboard.BorderColor = System.Drawing.Color.Transparent;
            this.rbtnDashboard.BorderRadius = 7;
            this.rbtnDashboard.BorderSize = 0;
            this.rbtnDashboard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtnDashboard.FlatAppearance.BorderSize = 0;
            this.rbtnDashboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnDashboard.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnDashboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtnDashboard.Location = new System.Drawing.Point(12, 173);
            this.rbtnDashboard.Name = "rbtnDashboard";
            this.rbtnDashboard.Size = new System.Drawing.Size(147, 30);
            this.rbtnDashboard.TabIndex = 0;
            this.rbtnDashboard.Text = "Dashboard";
            this.rbtnDashboard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtnDashboard.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtnDashboard.UseVisualStyleBackColor = false;
            this.rbtnDashboard.Click += new System.EventHandler(this.rbtnDashboard_Click);
            this.rbtnDashboard.MouseEnter += new System.EventHandler(this.rbtnDashboard_MouseEnter);
            this.rbtnDashboard.MouseLeave += new System.EventHandler(this.rbtnDashboard_MouseLeave);
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 10;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // CustomerMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 487);
            this.Controls.Add(this.pnlContainer);
            this.Controls.Add(this.panel1);
            this.Name = "CustomerMain";
            this.Text = "CustomerMain";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlContainer;
        private System.Windows.Forms.Panel panel1;
        private Properties.RoundButton rbtn_Exit;
        private Properties.RoundButton rbtn_Bookings;
        private Properties.RoundButton rbtn_MyTickets;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private Properties.RoundButton rbtnDashboard;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private Properties.RoundButton rbtn_Review;
        private Properties.RoundButton rbtn_Payments;
    }
}