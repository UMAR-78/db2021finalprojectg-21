﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Travel_Agency_Management_System;
using  Travel_Agency_Management_System.BL.AdminSection;
using Travel_Agency_Management_System.DL.Admin;
using Travel_Agency_Management_System.Forms.Customer;

namespace Travel_Agency_Management_System.Forms.Customer
{
    public partial class ucDashboard : UserControl
    {
        private static ucDashboard _instance;
        public static ucDashboard Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucDashboard();
                }
                return _instance;
            }
        }

        public void RefreshUI()
        {
           // DL.Admin.Admin_TripsClassDL.load_Packages_from_DataBase();

        }
        public ucDashboard()
        {
            InitializeComponent();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
        private void GenerateDynamicUserControl(string search="")
        {
            //flowLayoutPanel1.Controls.Clear();
            panel1.AutoScroll = true;
            panel1.Controls.Clear();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Packages", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            ucPackages[] listItems = new ucPackages[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string packageName = dt.Rows[i]["PackageName"].ToString();
                int package_price = (int)dt.Rows[i]["PackagePrice"];
                string departure = dt.Rows[i]["Departure"].ToString();
                int no_of_days = (int)dt.Rows[i]["NoofDays"];
                string destination = dt.Rows[i]["Destination"].ToString();
                byte[] imageData = (byte[])dt.Rows[i]["Picture"];
                Image picture;
                using (MemoryStream ms = new MemoryStream(imageData))
                {

                    Image pic = Image.FromStream(ms);
                    picture = pic;
                }

                if (packageName.Contains(search) || departure.Contains(search)||search.Equals("Search here")){

                    listItems[i] = new ucPackages();
                    listItems[i].PackageName = packageName;
                    listItems[i].Price = package_price;
                    listItems[i].Departure = departure;
                    listItems[i].Days = no_of_days;
                    listItems[i].Destination = destination;
                    listItems[i].Pic = picture;

                    panel1.Controls.Add(listItems[i]);
                    listItems[i].Dock = DockStyle.Top;
                }


               
            }

            //// Wrap the flowLayoutPanel1 inside a Panel with AutoScroll = true


            //this.Controls.Add(panel1);
        }


        private void ucDashboard_Load(object sender, EventArgs e)
        {
            GenerateDynamicUserControl();
        }

        private void txtbox_Search_TextChanged(object sender, EventArgs e)
        {
            GenerateDynamicUserControl(txtbox_Search.Text);
        }

        private void txtbox_Search_Enter(object sender, EventArgs e)
        {
            if(txtbox_Search.Text.Equals("Search here"))
            {
                txtbox_Search.Text = "";
            }
        }

        private void txtbox_Search_Leave(object sender, EventArgs e)
        {
            if (txtbox_Search.Text.Equals(""))
            {
                txtbox_Search.Text = "Search here";
            }
        }
    }
}








      






  
        

       
