﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Travel_Agency_Management_System.Forms.Customer
{
    public partial class ucReview : UserControl
    {
        private static ucReview _instance;
        public static ucReview Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucReview();
                }
                return _instance;
            }
        }

        public void RefreshUI()
        {

        }
        public ucReview()
        {
            InitializeComponent();
        }
    }
}
