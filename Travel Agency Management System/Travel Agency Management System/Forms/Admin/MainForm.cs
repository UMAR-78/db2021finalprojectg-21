﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Travel_Agency_Management_System.BL.AdminSection;
using Travel_Agency_Management_System.DL.Admin;
using Travel_Agency_Management_System.Forms.Admin;
using Travel_Agency_Management_System.Properties;
using Travel_Agency_Management_System.Utilities;

namespace Travel_Agency_Management_System
{
    public partial class MainForm : Form
    {
        private static MainForm _instance;
        public static MainForm Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MainForm();
                }
                return _instance;
            }
        }

        private static List<RoundButton> rbtnHoverTrue = new List<RoundButton>();
        private static List<RoundButton> rbtnHoverFalse = new List<RoundButton>();
       
        public static List<RoundButton> RbtnHoverFalse { get => rbtnHoverFalse; set => rbtnHoverFalse = value; }
        public static List<RoundButton> RbtnHoverTrue1 { get => rbtnHoverTrue; set => rbtnHoverTrue = value; }

        public MainForm()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
 

        private void pnlContainer_Paint(object sender, PaintEventArgs e)
        {

        }

        //Code of triggering hover 

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (RbtnHoverTrue1.Count < 1)
            {
                timer1.Stop();
            }
            else
            {
                for (int i = 0; i < RbtnHoverTrue1.Count; i++)
                {
                    if (RbtnHoverTrue1[i].Size.Height < 36)
                    {
                        RbtnHoverTrue1[i].Size = new Size(RbtnHoverTrue1[i].Size.Width + 1, RbtnHoverTrue1[i].Size.Height + 1);
                    }
                    else
                    {
                        RbtnHoverFalse.Add(RbtnHoverTrue1[i]);
                        RbtnHoverTrue1.Remove(RbtnHoverTrue1[i]);
                    }
                }
            }

            if (RbtnHoverTrue1.Count == 0)
            {
                timer1.Stop();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (RbtnHoverFalse.Count < 1)
            {
                timer2.Stop();
            }
            else
            {
                for (int i = 0; i < RbtnHoverFalse.Count; i++)
                {
                    if (RbtnHoverFalse[i].Size.Height > 30)
                    {
                        RbtnHoverFalse[i].Size = new Size(RbtnHoverFalse[i].Size.Width - 1, RbtnHoverFalse[i].Size.Height - 1);
                    }
                    else
                    {
                        RbtnHoverFalse.Remove(RbtnHoverFalse[i]);
                    }
                }
            }

            if (RbtnHoverFalse.Count == 0)
            {
                timer2.Stop();
            }
        }

        private void rbtnTrips_MouseEnter(object sender, EventArgs e)
        {
            timer2.Start();
            RoundButton button = (RoundButton)sender;
            if (RbtnHoverFalse.Contains(button))
            {
                RbtnHoverFalse.Remove(button);
            }
            button.Size = new Size(147, 30);
            RbtnHoverTrue1.Add(button);
            if (!timer1.Enabled)
            {
                timer1.Start();
            }
        }

        private void rbtnTrips_MouseLeave(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            timer2.Start();
            RoundButton button = (RoundButton)sender;
            if (RbtnHoverFalse.Contains(button))
            {
                RbtnHoverFalse.Remove(button);
            }
            button.Size = new Size(147, 30);
            RbtnHoverTrue1.Add(button);
            if (!timer1.Enabled)
            {
                timer1.Start();
            }
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void rbtn_Rooms_MouseEnter(object sender, EventArgs e)
        {
            timer2.Start();
            RoundButton button = (RoundButton)sender;
            if (RbtnHoverFalse.Contains(button))
            {
                RbtnHoverFalse.Remove(button);
            }
            button.Size = new Size(147, 30);
            RbtnHoverTrue1.Add(button);
            if (!timer1.Enabled)
            {
                timer1.Start();
            }
        }

        private void rbtn_Rooms_MouseLeave(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void rbtn_Vehicles_MouseEnter(object sender, EventArgs e)
        {
            timer2.Start();
            RoundButton button = (RoundButton)sender;
            if (RbtnHoverFalse.Contains(button))
            {
                RbtnHoverFalse.Remove(button);
            }
            button.Size = new Size(147, 30);
            RbtnHoverTrue1.Add(button);
            if (!timer1.Enabled)
            {
                timer1.Start();
            }
        }

        private void rbtn_Vehicles_MouseLeave(object sender, EventArgs e)
        {
            timer2.Start();
        }


        private void rbtn_Payments_MouseEnter(object sender, EventArgs e)
        {
            timer2.Start();
            RoundButton button = (RoundButton)sender;
            if (RbtnHoverFalse.Contains(button))
            {
                RbtnHoverFalse.Remove(button);
            }
            button.Size = new Size(147, 30);
            RbtnHoverTrue1.Add(button);
            if (!timer1.Enabled)
            {
                timer1.Start();
            }
        }
        private void rbtn_Payments_MouseLeave(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void rbtn_Reports_MouseEnter(object sender, EventArgs e)
        {
            timer2.Start();
            RoundButton button = (RoundButton)sender;
            if (RbtnHoverFalse.Contains(button))
            {
                RbtnHoverFalse.Remove(button);
            }
            button.Size = new Size(147, 30);
            RbtnHoverTrue1.Add(button);
            if (!timer1.Enabled)
            {
                timer1.Start();
            }
        }

        private void rbtn_Reports_MouseLeave(object sender, EventArgs e)
        {

            timer2.Start();
        }

        private void rbtn_Exit_MouseEnter(object sender, EventArgs e)
        {
            timer2.Start();
            RoundButton button = (RoundButton)sender;
            if (RbtnHoverFalse.Contains(button))
            {
                RbtnHoverFalse.Remove(button);
            }
            button.Size = new Size(147, 30);
            RbtnHoverTrue1.Add(button);
            if (!timer1.Enabled)
            {
                timer1.Start();
            }
        }

        private void rbtn_Exit_MouseLeave(object sender, EventArgs e)
        {

            timer2.Start();
        }


        //Buttons Click Events Code in navigate class in Utilities
        private void roundButton1_Click(object sender, EventArgs e) => Navigate.ToTrips();
        private void rbtn_Hotel_Click(object sender, EventArgs e) => Navigate.ToHotels();
        private void rbtn_Rooms_Click(object sender, EventArgs e) => Navigate.ToHotelRooms();
        private void rbtn_Vehicles_Click(object sender, EventArgs e)=>Navigate.ToVehicles();
        private void rbtn_Payments_Click(object sender, EventArgs e) => Navigate.ToPayments();
        private void rbtn_Reports_Click(object sender, EventArgs e) => Navigate.ToReports();
        private void rbtn_Exit_Click(object sender, EventArgs e)
        {
            Login newForm = new Login();
            newForm.Show();
            this.Hide();
        }
        
       

        private void MainForm_Load(object sender, EventArgs e)
        {
            Admin_HotelClassDL.load_data_from_DataBase();
            Admin_VehiclesClassDL.load_VEHICLE_data_from_DataBase();
            Admin_PaymentsClassDL.load_Payments_data_from_DataBase();
        }

        private void roundButton1_Click_1(object sender, EventArgs e) => Navigate.ToViewHotelPackages();

        private void roundButton2_Click(object sender, EventArgs e) => Navigate.ToViewHotelROOMs();

































































        // Trips button code

    }
}
