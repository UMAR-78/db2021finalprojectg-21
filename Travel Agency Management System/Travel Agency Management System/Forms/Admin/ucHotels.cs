﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using Travel_Agency_Management_System.BL.AdminSection;
using Travel_Agency_Management_System.DL.Admin;
using System.Data.SqlClient;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;
using System.Diagnostics;
using System.Xml.Linq;

namespace Travel_Agency_Management_System.Forms.Admin
{
    public partial class ucHotels : UserControl
    {
        private static ucHotels _instance;
        private string name = "";
        private int price = 0;
        private string location = "";
        private string review = "";

        public static ucHotels Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucHotels();
                }
                return _instance;
            }
        }
        private ucHotels()
        {
            InitializeComponent();
        }
        public void RefreshUI()
        {

        }


        //Save Btn code for Hotel
        private void rbtnSave_Click(object sender, EventArgs e)
        {
            ValidateInputFields();
            if (!label9.Visible && !label10.Visible && !label11.Visible && !label12.Visible)
            {

                Admin_HotelClass hotelsclass = new Admin_HotelClass(txtox_hotelName.Text, int.Parse(txtBox_HotelPrice.Text), txtBox_HotelLocation.Text, txtBox_HotelReview.Text);

                if (editlbl.Text != "")
                {
                    Admin_HotelClass hotels = new Admin_HotelClass(name, price, location, review);
                    bool n = Admin_HotelClassDL.checkitsexist(hotelsclass, hotels);
                    if (n)
                    {


                        string query = "UPDATE hotels SET name = @name, price = @price, location = @location, review = @review WHERE name =@uname and price=@uprice and location=@ulocation and review=@ureview";
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand(query, con);
                        cmd.Parameters.AddWithValue("@name", hotelsclass.HotelName);
                        cmd.Parameters.AddWithValue("@price", hotelsclass.Price);
                        cmd.Parameters.AddWithValue("@location", hotelsclass.Location);
                        cmd.Parameters.AddWithValue("@review", hotelsclass.Review);
                        cmd.Parameters.AddWithValue("@uname", hotels.HotelName);
                        cmd.Parameters.AddWithValue("@uprice", hotels.Price);
                        cmd.Parameters.AddWithValue("@ulocation", hotels.Location);
                        cmd.Parameters.AddWithValue("@ureview", hotels.Review);



                        cmd.ExecuteNonQuery();

                        MessageBox.Show("Updated data !");
                        editlbl.Text = "";
                    }


                }
                else
                {
                    Admin_HotelClassDL.Add_hotel_data_in_List(hotelsclass);
                    Admin_HotelClassDL.AddHotelToDatabase(hotelsclass);

                }

                bindata();
                clearbox();
                ClearErrorLabels();
            }

        }
        private void bindata()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Admin_HotelClassDL.HotelClassList2;
            dataGridView1.Refresh();
        }

       


       

        private void  ClearErrorLabels()
        {
            label9.Text = "";
            label10.Text = "";
            label11.Text = "";
            label12.Text = "";

        }
        private void clearbox()
        {
            txtBox_HotelLocation.Text = "";
            txtBox_HotelPrice.Text = "";
            txtBox_HotelReview.Text = "";
            txtox_hotelName.Text = "";

        }

        private void ValidateInputFields()
        {
            // Validate=ion for Name 
            if (!Regex.IsMatch(txtox_hotelName.Text, @"^[a-zA-Z]+(\s[a-zA-Z]+)*\s*$"))
            {
                label9.Visible = true;

            }
            else
            {
                label9.Visible = false;
            }

            // Validation for HotelRevire
            if (!Regex.IsMatch(txtBox_HotelReview.Text, @"^[a-zA-Z]+(\s[a-zA-Z]+)*\s*$"))
            {
                label12.Visible = true;

            }
            else
            {
                label12.Visible = false;
            }


            //Validation for Price
            if (!Regex.IsMatch(txtBox_HotelPrice.Text, @"^[1-9]\d*$"))
            {
                label10.Visible = true;

            }
            else
            {
                label10.Visible = false;
            }


            //Validatino for Locatin
            if (!Regex.IsMatch(txtBox_HotelLocation.Text, @"^[a-zA-Z]+(\s[a-zA-Z]+)*\s*$"))
            {
                label11.Visible = true;

            }
            else
            {
                label11.Visible = false;
            }

           

        }



        //To remove validations if text is correct
        private void txtox_hotelName_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }

        private void txtBox_HotelPrice_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }

        private void txtBox_HotelLocation_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }

        private void txtBox_HotelReview_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }

       
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns["Delete"].Index && e.RowIndex != -1)
            {
                // Get the corresponding DataGridViewRow object
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];

                // Get the values of the cells in the row
                string name = Convert.ToString(row.Cells[2].Value);
                int price = Convert.ToInt32(row.Cells[3].Value);
                string location = Convert.ToString(row.Cells[4].Value);
                string review = Convert.ToString(row.Cells[5].Value);

                // Delete the hotel from the list and the database
                Admin_HotelClass pakro = (Admin_HotelClass)row.DataBoundItem;
                Admin_HotelClassDL.delete_hotel_from_list(pakro);

                var con = Configuration.getInstance().getConnection();
                string procedureName = "sp_DeleteHotel";
                SqlCommand cmd = new SqlCommand(procedureName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@Price", price);
                cmd.Parameters.AddWithValue("@Location", location);
                cmd.Parameters.AddWithValue("@Review", review);
                cmd.ExecuteNonQuery();


                MessageBox.Show("Deleted");
                bindata();
            }
            if (e.ColumnIndex == dataGridView1.Columns["Edit"].Index && e.RowIndex != -1)
            {
                txtox_hotelName.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
                txtBox_HotelPrice.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
                txtBox_HotelLocation.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
                txtBox_HotelReview.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();

                name = txtox_hotelName.Text;
                price = int.Parse(txtBox_HotelPrice.Text);
                location = txtBox_HotelLocation.Text;
                review = txtBox_HotelReview.Text;

                editlbl.Text = "In Edit mode";
            }

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void ucHotels_Load(object sender, EventArgs e)
        {
            
            dataGridView1.DataSource = Admin_HotelClassDL.HotelClassList2;
        }
    }

}
