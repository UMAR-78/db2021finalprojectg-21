﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Travel_Agency_Management_System.BL.AdminSection;
using Travel_Agency_Management_System.DL.Admin;

namespace Travel_Agency_Management_System.Forms.Admin
{
    public partial class ucPayments : UserControl
    {
        private static ucPayments _instance;
        private string methods;
        public static ucPayments Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucPayments();
                }
                return _instance;
            }
        }

        public void RefreshUI()
        {

        }
        public ucPayments()
        {
            InitializeComponent();
        }
        private void ucPayments_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Admin_PaymentsClassDL.PaymentsClassList1;
        }

        private void rbtnSave_Click(object sender, EventArgs e)
        {
            ValidateInputFields();
            if (!label11.Visible)
            {

                Admin_PaymentsClass Paymentsclass = new Admin_PaymentsClass(txtbox_Tname.Text);

                if (editlbl.Text != "")
                {
                    Admin_PaymentsClass payments = new Admin_PaymentsClass(methods);
                    bool n = Admin_PaymentsClassDL.checkitsexist(Paymentsclass, payments);
                    if (n)
                    {


                        string query = "UPDATE Payments SET methods = @methodsWHERE methods =@umethods ";
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand(query, con);
                        cmd.Parameters.AddWithValue("@methods", Paymentsclass.Paymenent_method);
                        cmd.Parameters.AddWithValue("@umethods", payments.Paymenent_method);
                 
                        cmd.ExecuteNonQuery();

                        MessageBox.Show("Updated data !");
                        editlbl.Text = "";
                    }


                }
                else
                {
                    Admin_PaymentsClassDL.Add_payment_data_in_List(Paymentsclass);
                    Admin_PaymentsClassDL.AddPaymentsToDatabase(Paymentsclass);

                }

                bindata();
                clearbox();
                ClearErrorLabels();
            }

        }

        private void bindata()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Admin_PaymentsClassDL.PaymentsClassList1;
            dataGridView1.Refresh();
        }

        private void ClearErrorLabels()
        {
            label11.Text = "";
        }
        private void clearbox()
        {
            txtbox_Tname.Text = "";
           
        }

        private void ValidateInputFields()
        {
            // Validate=ion for Name 
            if (!Regex.IsMatch(txtbox_Tname.Text, @"^[a-zA-Z]+(\s[a-zA-Z]+)*\s*$"))
            {
                label11.Visible = true;

            }
            else
            {
                label11.Visible = false;
            }

 
        }

        private void txtbox_Tname_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns["Delete"].Index && e.RowIndex != -1)
            {
                // Get the corresponding DataGridViewRow object
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];

                // Get the values of the cells in the row
                string methodname = Convert.ToString(row.Cells[1].Value);
                

                // Delete the hotel from the list and the database
                Admin_PaymentsClass pakro = (Admin_PaymentsClass)row.DataBoundItem;
                Admin_PaymentsClassDL.delete_payment_from_list(pakro);

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("sp_DeletePayment", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@methods", methodname);
                cmd.ExecuteNonQuery();


                MessageBox.Show("Deleted");
                bindata();
            }

        }
    }
}
