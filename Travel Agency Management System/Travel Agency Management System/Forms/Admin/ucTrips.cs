﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Travel_Agency_Management_System.Properties;

namespace Travel_Agency_Management_System
{
    public partial class ucTrips : UserControl
    {
        private static List<RoundButton> rbtnHoverTrue = new List<RoundButton>();
        private static List<RoundButton> rbtnHoverFalse = new List<RoundButton>();

        public static List<RoundButton> RbtnHoverFalse { get => rbtnHoverFalse; set => rbtnHoverFalse = value; }
        public static List<RoundButton> RbtnHoverTrue1 { get => rbtnHoverTrue; set => rbtnHoverTrue = value; }
        private static ucTrips _instance;
        public static ucTrips Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucTrips();
                }
                return _instance;
            }
        }
        public ucTrips()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }


        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ucTrips_Load(object sender, EventArgs e)
        {
            load_data_from_DataBase();
        }

        public void RefreshUI()
        {

        }
















        // This function is used to validate input fields
        private void ValidateInputFields()
        {
            // Validate=ion for Name 
            if (!Regex.IsMatch(txtbox_Tname.Text, @"^[a-zA-Z]+(\s[a-zA-Z]+)*\s*$"))
            {
                label11.Visible = true;

            }
            else
            {
                label11.Visible = false;
            }

            // Validation for Description



            //Validation for Price
            if (!Regex.IsMatch(txtbox_Tprice.Text, @"^[1-9]\d*$"))
            {
                label13.Visible = true;

            }
            else
            {
                label13.Visible = false;
            }


            //Validatino for Dyas
            if (!Regex.IsMatch(txtbox_TDays.Text, @"^[1-9]\d*$"))
            {
                label14.Visible = true;

            }
            else
            {
                label14.Visible = false;
            }

            //Validation for Departure
            if (!Regex.IsMatch(txtBox_tDeparture.Text, @"^[a-zA-Z]+(\s[a-zA-Z]+)*\s*$"))
            {
                label15.Visible = true;

            }
            else
            {
                label15.Visible = false;
            }


            //Validation for Destination
            if (!Regex.IsMatch(txtbox_TDestination.Text, @"^[a-zA-Z]+(\s[a-zA-Z]+)*\s*$"))
            {
                label16.Visible = true;

            }
            else
            {
                label16.Visible = false;
            }

        }




        //Red line error tab tak rhyn gy jb tak sahi data enter na kia jay
        private void txtbox_Tdescription_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();

        }

        private void txtbox_Tname_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }

        private void txtbox_Tprice_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }

        private void txtbox_TDays_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }

        private void txtBox_tDeparture_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }

        private void txtbox_TDestination_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }

        private void roundButton2_Click(object sender, EventArgs e)
        {
            OpenFileDialog opendlg = new OpenFileDialog();
            if (opendlg.ShowDialog() == DialogResult.OK)
            {
                Image image = Image.FromFile(opendlg.FileName);
                pb_image.Image = image;
            }
        }




        ///Save btn code starts from here
        private void roundButton1_Click(object sender, EventArgs e)
        {
            ValidateInputFields();
            if (!label11.Visible &&
                !label13.Visible &&
                !label14.Visible &&
                !label15.Visible && !label16.Visible )

            {
                if (pb_image.Image != null)
                {
                    savedata();
                    clear_textbox();

                }
                else
                {
                    MessageBox.Show("Please select an image.");

                }
            }

        }

        
        //--------------------------------Code to clear data from input fields-----------------------------
        private void clear_textbox()
        {
            txtbox_TDays.Text = "";
            txtBox_tDeparture.Text = "";
            txtbox_TDestination.Text = "";
            txtbox_Tname.Text = "";
            txtbox_Tprice.Text = "";


        }

        private void savedata()
        {
            var con = Configuration.getInstance().getConnection();

            // Check if the same package has already been entered
            SqlCommand cmdCheck = new SqlCommand("SELECT COUNT(*) FROM Packages WHERE PackageName=@PackageName AND PackagePrice=@PackagePrice AND Departure=@Departure AND NoofDays=@NoofDays ANd Destination = @Destination", con);
            cmdCheck.Parameters.AddWithValue("@PackageName", txtbox_Tname.Text);
            cmdCheck.Parameters.AddWithValue("@PackagePrice", txtbox_Tprice.Text);
            cmdCheck.Parameters.AddWithValue("@Departure", txtBox_tDeparture.Text);
            cmdCheck.Parameters.AddWithValue("@NoofDays", txtbox_TDays.Text);
           // cmdCheck.Parameters.AddWithValue("@Picture", imageBytes);
            cmdCheck.Parameters.AddWithValue("@Destination", txtbox_TDestination.Text);


            int count = (int)cmdCheck.ExecuteScalar();

            if (count > 0)
            {
                MessageBox.Show("This package has already been entered. Please enter a new one.");
            }
            else
            {
                // Upload picture to database
                byte[] imageBytes = null;
                if (pb_image.Image != null)
                {
                    // Convert an image to byte[] 
                    //byte[] imageData;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        pb_image.Image.Save(ms, pb_image.Image.RawFormat);
                        imageBytes = ms.ToArray();
                    }
                }

                // Insert data into database
                SqlCommand cmd = new SqlCommand("INSERT INTO Packages (PackageName, PackagePrice, Departure, Picture,Destination, NoofDays) VALUES (@PackageName, @PackagePrice, @Departure, @Picture,@Destination, @NoofDays)", con);
                cmd.Parameters.AddWithValue("@PackageName", txtbox_Tname.Text);
                cmd.Parameters.AddWithValue("@PackagePrice", txtbox_Tprice.Text);
                cmd.Parameters.AddWithValue("@Departure", txtBox_tDeparture.Text);
                cmd.Parameters.AddWithValue("@NoofDays", txtbox_TDays.Text);
                cmd.Parameters.AddWithValue("@Picture", imageBytes);
            //    cmd.Parameters.AddWithValue("@Id", 1);

                cmd.Parameters.AddWithValue("@Destination", txtbox_TDestination.Text);


                cmd.ExecuteNonQuery();
                MessageBox.Show("Package added successfully!");
            }
        }


        private void load_data_from_DataBase()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Packages", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }

    }
}