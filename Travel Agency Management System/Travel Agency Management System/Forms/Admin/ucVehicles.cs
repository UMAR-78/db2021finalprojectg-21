﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Travel_Agency_Management_System.BL.AdminSection;
using Travel_Agency_Management_System.DL.Admin;

namespace Travel_Agency_Management_System.Forms.Admin
{
    public partial class ucVehicles : UserControl
    {
        private static ucVehicles _instance;


        private string type = "";
        private int price = 0;
        private int seats = 0;
        
        private string model = "";
        public static ucVehicles Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucVehicles();
                }
                return _instance;
            }
        }

        public void RefreshUI()
        {

        }
        private ucVehicles()
        {
            InitializeComponent();
        }



        //Save buttin code
        private void rbtnSave_Click(object sender, EventArgs e)
        {
            ValidateInputFields();
            if (!label11.Visible && !label12.Visible && !label13.Visible && !label14.Visible)
            {

                Admin_VehiclesClass vehicleclass = new Admin_VehiclesClass(comboBox_Type.SelectedItem.ToString(),txtbox_Model.Text, int.Parse(txtbox_Seats.Text), int.Parse(txtbox_Price.Text));

                if (editlbl.Text != "")
                {
                    Admin_VehiclesClass vehicle = new Admin_VehiclesClass(type, model, seats, price);
               //     MessageBox.Show(type + model + seats + price) ;
                    bool n = Admin_VehiclesClassDL.checkitsexist(vehicleclass, vehicle);
                    if (n)
                    {
                        var con = Configuration.getInstance().getConnection();
                        string query = "UPDATE Vehicle SET vehicle_type = @vehicle_type, price_per_seat = @price_per_seat, model = @model, total_seats = @total_seats WHERE vehicle_type = @uvehicle_type and price_per_seat = @uprice_per_seat and model = @umodel and total_seats = @utotal_seats";
                        SqlCommand cmd = new SqlCommand(query, con);
                        cmd.Parameters.AddWithValue("@vehicle_type", vehicleclass.Vehicle_type);
                        cmd.Parameters.AddWithValue("@price_per_seat", vehicleclass.Price_per_seat);
                        cmd.Parameters.AddWithValue("@model", vehicleclass.Vehicle_model);
                        cmd.Parameters.AddWithValue("@total_seats", vehicleclass.No_of_seats);
                        cmd.Parameters.AddWithValue("@uvehicle_type", vehicle.Vehicle_type);
                        cmd.Parameters.AddWithValue("@uprice_per_seat", vehicle.Price_per_seat);
                        cmd.Parameters.AddWithValue("@umodel", vehicle.Vehicle_model);
                        cmd.Parameters.AddWithValue("@utotal_seats", vehicle.No_of_seats);

                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Updated data !");
                        editlbl.Text = "";
                    }
                }

                else
                {
                    Admin_VehiclesClassDL.Add_vehicle_data_in_List(vehicleclass);
                    Admin_VehiclesClassDL.AddVechilesToDatabase(vehicleclass);

                }

                bindata();
                clearbox();
                ClearErrorLabels();
            }

        }


        private void bindata()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Admin_VehiclesClassDL.VehicleClassList1;
            dataGridView1.Refresh();
        }






        private void ClearErrorLabels()
        {
            label11.Text = "";
            label12.Text = "";
            label13.Text = "";
            label14.Text = "";

        }
        private void clearbox()
        {
            txtbox_Model.Text = "";
            txtbox_Price.Text = "";
            txtbox_Seats.Text = "";


        }
        private void ValidateInputFields()
        {

            if (!Regex.IsMatch(txtbox_Model.Text, @"^[1-9]\d*$"))
            {
                label12.Visible = true;

            }
            else
            {
                label12.Visible = false;
            }


            //Validation for Price
            if (!Regex.IsMatch(txtbox_Price.Text, @"^[1-9]\d*$"))
            {
                label13.Visible = true;

            }
            else
            {
                label13.Visible = false;
            }


            //Validatino for seats
            if (!Regex.IsMatch(txtbox_Seats.Text, @"^[1-9]\d*$"))
            {
                label14.Visible = true;

            }
            else
            {
                label14.Visible = false;
            }



        }

        private void txtbox_Model_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }

        private void txtbox_Seats_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }

        private void txtbox_Price_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == dataGridView1.Columns["Delete"].Index && e.RowIndex != -1)
            {
                // Get the corresponding DataGridViewRow object
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];

                // Get the values of the cells in the row
                string type = Convert.ToString(row.Cells[2].Value);
                int model = Convert.ToInt32(row.Cells[3].Value);
                int seats = Convert.ToInt32(row.Cells[4].Value);
                int price = Convert.ToInt32(row.Cells[5].Value);

                // Delete the vehicle from the list and the database
                Admin_VehiclesClass vehicle = (Admin_VehiclesClass)row.DataBoundItem;
                Admin_VehiclesClassDL.delete_vehicle_from_list(vehicle);

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("sp_DeleteVehicle", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@vehicle_type", type);
                cmd.Parameters.AddWithValue("@model", model);
                cmd.Parameters.AddWithValue("@total_seats", seats);
                cmd.Parameters.AddWithValue("@price_per_seat", price);
                cmd.ExecuteNonQuery();


                MessageBox.Show("Vehicle deleted successfully!");
                bindata();
            }
            else if (e.ColumnIndex == dataGridView1.Columns["Edit"].Index && e.RowIndex != -1)
            {
                // Get the corresponding DataGridViewRow object
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];

                // Get the values of the cells in the row
                type = Convert.ToString(row.Cells[2].Value);
                model = Convert.ToString(row.Cells[3].Value);
                seats = Convert.ToInt32(row.Cells[4].Value);
                price = Convert.ToInt32(row.Cells[5].Value);

                // Set the edit label
                editlbl.Text = "Edit " + type + " Vehicle";

                // Set the input field values
                comboBox_Type.SelectedItem = type;
                txtbox_Model.Text = model.ToString();
                txtbox_Seats.Text = seats.ToString();
                txtbox_Price.Text = price.ToString();
            }


        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void ucVehicles_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Admin_VehiclesClassDL.VehicleClassList1;
        }
    }
}
