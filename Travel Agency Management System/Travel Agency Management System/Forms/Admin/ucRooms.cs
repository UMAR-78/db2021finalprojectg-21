﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Travel_Agency_Management_System.Forms.Admin
{
    public partial class ucRooms : UserControl
    {
        private static ucRooms _instance;
        public static ucRooms Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucRooms();
                }
                return _instance;
            }
        }
        private ucRooms()
        {
            InitializeComponent();
        }

       
        public void RefreshUI()
        {

        }

        private void ucRooms_Load(object sender, EventArgs e)
        {
            GETHOTELidFromHotelTable();
            load_data_from_DataBase();
        }



        //Save btn code 
        private void roundButton1_Click(object sender, EventArgs e)
        {
            ValidateInputFields();

            if (!label13.Visible && !label14.Visible)
            {
                if (comboBox_HID.SelectedItem != null)
                {
                    if (comboBoxHName.SelectedItem != null)
                    {
                        saveData();
                        load_data_from_DataBase();
                        clear_textbox();
                        removeValidation();
                    }
                    else
                    {
                        MessageBox.Show("Please select Hotel name");
                    }

                }
                else
                {
                    MessageBox.Show("Please select Hotel Id from Combobox");
                }
            }
           

        }
        private void saveData()
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Insert into HotelRooms (HotelId, HotelName, NoofRooms, NoofBeds) values (@HotelId, @HotelName, @NoofRooms, @NoofBeds)", con);

            cmd.Parameters.AddWithValue("@HotelId", comboBox_HID.SelectedItem);
            cmd.Parameters.AddWithValue("@HotelName", comboBoxHName.SelectedItem);
            cmd.Parameters.AddWithValue("@NoofRooms", txtbox_NoRooms.Text);
            cmd.Parameters.AddWithValue("@NoofBeds", txtbox_TBedsInRooms.Text);

            cmd.ExecuteNonQuery();
            MessageBox.Show("Data saved successfully!");
        }


        private void load_data_from_DataBase()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from HotelRooms", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void clear_textbox()
        {
            txtbox_NoRooms.Text = "";
            txtbox_TBedsInRooms.Text = "";
        }

        private void removeValidation()
        {
            label13.Text = "";
            label14.Text = "";
        }

        private void ValidateInputFields()
        {
            // Validate=ion for ID
            if (comboBox_HID.SelectedItem.ToString() == "")
            {
                label12.Visible = true;

            }
            else
            {
                label12.Visible = false;
            }

            // Validation for HotelName
            if (comboBoxHName.SelectedItem.ToString() == "")
            {
                label11.Visible = true;

            }
            else
            {
                label11.Visible = false;
            }


            //Validation for Rooms
            if (!Regex.IsMatch(txtbox_NoRooms.Text, @"^[1-9]\d*$"))
            {
                label13.Visible = true;

            }
            else
            {
                label13.Visible = false;
            }


            //Validatino for beds in rooms
            if (!Regex.IsMatch(txtbox_TBedsInRooms.Text, @"^[1-9]\d*$"))
            {
                label14.Visible = true;

            }
            else
            {
                label14.Visible = false;
            }



        }

        private void txtbox_NoRooms_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }

        private void txtbox_TBedsInRooms_TextChanged(object sender, EventArgs e)
        {
            ValidateInputFields();
        }


        private void GETHOTELidFromHotelTable()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select HotelId , Name from Hotels", con);

            SqlDataReader reader = cmd.ExecuteReader();
            // Populate the combobox with the retrieved data
            while (reader.Read())
            {
                comboBox_HID.Items.Add((int)reader["HotelId"]);
                comboBoxHName.Items.Add(reader["Name"]);
            }

            // Close the database connection
            reader.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
            
        }
    }

}
