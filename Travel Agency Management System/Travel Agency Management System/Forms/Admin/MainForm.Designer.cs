﻿
namespace Travel_Agency_Management_System
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.roundButton2 = new Travel_Agency_Management_System.Properties.RoundButton();
            this.roundButton1 = new Travel_Agency_Management_System.Properties.RoundButton();
            this.rbtn_Exit = new Travel_Agency_Management_System.Properties.RoundButton();
            this.rbtn_Reports = new Travel_Agency_Management_System.Properties.RoundButton();
            this.rbtn_Payments = new Travel_Agency_Management_System.Properties.RoundButton();
            this.rbtn_Vehicles = new Travel_Agency_Management_System.Properties.RoundButton();
            this.rbtn_Rooms = new Travel_Agency_Management_System.Properties.RoundButton();
            this.rbtn_Hotel = new Travel_Agency_Management_System.Properties.RoundButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.rbtnTrips = new Travel_Agency_Management_System.Properties.RoundButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.pnlContainer = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.panel1.Controls.Add(this.roundButton2);
            this.panel1.Controls.Add(this.roundButton1);
            this.panel1.Controls.Add(this.rbtn_Exit);
            this.panel1.Controls.Add(this.rbtn_Reports);
            this.panel1.Controls.Add(this.rbtn_Payments);
            this.panel1.Controls.Add(this.rbtn_Vehicles);
            this.panel1.Controls.Add(this.rbtn_Rooms);
            this.panel1.Controls.Add(this.rbtn_Hotel);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.rbtnTrips);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(260, 969);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // roundButton2
            // 
            this.roundButton2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.roundButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.roundButton2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.roundButton2.BorderColor = System.Drawing.Color.Transparent;
            this.roundButton2.BorderRadius = 7;
            this.roundButton2.BorderSize = 0;
            this.roundButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.roundButton2.FlatAppearance.BorderSize = 0;
            this.roundButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundButton2.Font = new System.Drawing.Font("Arial Black", 9.75F, System.Drawing.FontStyle.Bold);
            this.roundButton2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.roundButton2.Location = new System.Drawing.Point(20, 711);
            this.roundButton2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.roundButton2.Name = "roundButton2";
            this.roundButton2.Size = new System.Drawing.Size(220, 78);
            this.roundButton2.TabIndex = 37;
            this.roundButton2.Text = "View of hotel and hotel rooms";
            this.roundButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.roundButton2.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.roundButton2.UseVisualStyleBackColor = false;
            this.roundButton2.Click += new System.EventHandler(this.roundButton2_Click);
            // 
            // roundButton1
            // 
            this.roundButton1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.roundButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.roundButton1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.roundButton1.BorderColor = System.Drawing.Color.Transparent;
            this.roundButton1.BorderRadius = 7;
            this.roundButton1.BorderSize = 0;
            this.roundButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.roundButton1.FlatAppearance.BorderSize = 0;
            this.roundButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundButton1.Font = new System.Drawing.Font("Arial Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roundButton1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.roundButton1.Location = new System.Drawing.Point(18, 629);
            this.roundButton1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.roundButton1.Name = "roundButton1";
            this.roundButton1.Size = new System.Drawing.Size(220, 72);
            this.roundButton1.TabIndex = 36;
            this.roundButton1.Text = "View of Hotels Packages";
            this.roundButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.roundButton1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.roundButton1.UseVisualStyleBackColor = false;
            this.roundButton1.Click += new System.EventHandler(this.roundButton1_Click_1);
            // 
            // rbtn_Exit
            // 
            this.rbtn_Exit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.rbtn_Exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Exit.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Exit.BorderColor = System.Drawing.Color.Transparent;
            this.rbtn_Exit.BorderRadius = 7;
            this.rbtn_Exit.BorderSize = 0;
            this.rbtn_Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtn_Exit.FlatAppearance.BorderSize = 0;
            this.rbtn_Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtn_Exit.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_Exit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.rbtn_Exit.Location = new System.Drawing.Point(18, 923);
            this.rbtn_Exit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbtn_Exit.Name = "rbtn_Exit";
            this.rbtn_Exit.Size = new System.Drawing.Size(220, 46);
            this.rbtn_Exit.TabIndex = 35;
            this.rbtn_Exit.Text = "Exit";
            this.rbtn_Exit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtn_Exit.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.rbtn_Exit.UseVisualStyleBackColor = false;
            this.rbtn_Exit.Click += new System.EventHandler(this.rbtn_Exit_Click);
            this.rbtn_Exit.MouseEnter += new System.EventHandler(this.rbtn_Exit_MouseEnter);
            this.rbtn_Exit.MouseLeave += new System.EventHandler(this.rbtn_Exit_MouseLeave);
            // 
            // rbtn_Reports
            // 
            this.rbtn_Reports.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rbtn_Reports.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Reports.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Reports.BorderColor = System.Drawing.Color.Transparent;
            this.rbtn_Reports.BorderRadius = 7;
            this.rbtn_Reports.BorderSize = 0;
            this.rbtn_Reports.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtn_Reports.FlatAppearance.BorderSize = 0;
            this.rbtn_Reports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtn_Reports.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_Reports.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.rbtn_Reports.Location = new System.Drawing.Point(20, 574);
            this.rbtn_Reports.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbtn_Reports.Name = "rbtn_Reports";
            this.rbtn_Reports.Size = new System.Drawing.Size(220, 46);
            this.rbtn_Reports.TabIndex = 34;
            this.rbtn_Reports.Text = "Reports";
            this.rbtn_Reports.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtn_Reports.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.rbtn_Reports.UseVisualStyleBackColor = false;
            this.rbtn_Reports.Click += new System.EventHandler(this.rbtn_Reports_Click);
            this.rbtn_Reports.MouseEnter += new System.EventHandler(this.rbtn_Reports_MouseEnter);
            this.rbtn_Reports.MouseLeave += new System.EventHandler(this.rbtn_Reports_MouseLeave);
            // 
            // rbtn_Payments
            // 
            this.rbtn_Payments.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rbtn_Payments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Payments.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Payments.BorderColor = System.Drawing.Color.Transparent;
            this.rbtn_Payments.BorderRadius = 7;
            this.rbtn_Payments.BorderSize = 0;
            this.rbtn_Payments.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtn_Payments.FlatAppearance.BorderSize = 0;
            this.rbtn_Payments.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtn_Payments.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_Payments.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.rbtn_Payments.Location = new System.Drawing.Point(18, 512);
            this.rbtn_Payments.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbtn_Payments.Name = "rbtn_Payments";
            this.rbtn_Payments.Size = new System.Drawing.Size(220, 46);
            this.rbtn_Payments.TabIndex = 33;
            this.rbtn_Payments.Text = "Payments";
            this.rbtn_Payments.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtn_Payments.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.rbtn_Payments.UseVisualStyleBackColor = false;
            this.rbtn_Payments.Click += new System.EventHandler(this.rbtn_Payments_Click);
            this.rbtn_Payments.MouseEnter += new System.EventHandler(this.rbtn_Payments_MouseEnter);
            this.rbtn_Payments.MouseLeave += new System.EventHandler(this.rbtn_Payments_MouseLeave);
            // 
            // rbtn_Vehicles
            // 
            this.rbtn_Vehicles.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rbtn_Vehicles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Vehicles.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Vehicles.BorderColor = System.Drawing.Color.Transparent;
            this.rbtn_Vehicles.BorderRadius = 7;
            this.rbtn_Vehicles.BorderSize = 0;
            this.rbtn_Vehicles.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtn_Vehicles.FlatAppearance.BorderSize = 0;
            this.rbtn_Vehicles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtn_Vehicles.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_Vehicles.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.rbtn_Vehicles.Location = new System.Drawing.Point(20, 451);
            this.rbtn_Vehicles.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbtn_Vehicles.Name = "rbtn_Vehicles";
            this.rbtn_Vehicles.Size = new System.Drawing.Size(220, 46);
            this.rbtn_Vehicles.TabIndex = 32;
            this.rbtn_Vehicles.Text = "Vehicles";
            this.rbtn_Vehicles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtn_Vehicles.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.rbtn_Vehicles.UseVisualStyleBackColor = false;
            this.rbtn_Vehicles.Click += new System.EventHandler(this.rbtn_Vehicles_Click);
            this.rbtn_Vehicles.MouseEnter += new System.EventHandler(this.rbtn_Vehicles_MouseEnter);
            this.rbtn_Vehicles.MouseLeave += new System.EventHandler(this.rbtn_Vehicles_MouseLeave);
            // 
            // rbtn_Rooms
            // 
            this.rbtn_Rooms.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rbtn_Rooms.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Rooms.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Rooms.BorderColor = System.Drawing.Color.Transparent;
            this.rbtn_Rooms.BorderRadius = 7;
            this.rbtn_Rooms.BorderSize = 0;
            this.rbtn_Rooms.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtn_Rooms.FlatAppearance.BorderSize = 0;
            this.rbtn_Rooms.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtn_Rooms.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_Rooms.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.rbtn_Rooms.Location = new System.Drawing.Point(20, 389);
            this.rbtn_Rooms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbtn_Rooms.Name = "rbtn_Rooms";
            this.rbtn_Rooms.Size = new System.Drawing.Size(220, 46);
            this.rbtn_Rooms.TabIndex = 31;
            this.rbtn_Rooms.Text = "Hotel Rooms";
            this.rbtn_Rooms.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtn_Rooms.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.rbtn_Rooms.UseVisualStyleBackColor = false;
            this.rbtn_Rooms.Click += new System.EventHandler(this.rbtn_Rooms_Click);
            this.rbtn_Rooms.MouseEnter += new System.EventHandler(this.rbtn_Rooms_MouseEnter);
            this.rbtn_Rooms.MouseLeave += new System.EventHandler(this.rbtn_Rooms_MouseLeave);
            // 
            // rbtn_Hotel
            // 
            this.rbtn_Hotel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rbtn_Hotel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Hotel.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtn_Hotel.BorderColor = System.Drawing.Color.Transparent;
            this.rbtn_Hotel.BorderRadius = 7;
            this.rbtn_Hotel.BorderSize = 0;
            this.rbtn_Hotel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtn_Hotel.FlatAppearance.BorderSize = 0;
            this.rbtn_Hotel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtn_Hotel.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_Hotel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.rbtn_Hotel.Location = new System.Drawing.Point(18, 328);
            this.rbtn_Hotel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbtn_Hotel.Name = "rbtn_Hotel";
            this.rbtn_Hotel.Size = new System.Drawing.Size(220, 46);
            this.rbtn_Hotel.TabIndex = 0;
            this.rbtn_Hotel.Text = "Hotels";
            this.rbtn_Hotel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtn_Hotel.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.rbtn_Hotel.UseVisualStyleBackColor = false;
            this.rbtn_Hotel.Click += new System.EventHandler(this.rbtn_Hotel_Click);
            this.rbtn_Hotel.MouseEnter += new System.EventHandler(this.button2_MouseEnter);
            this.rbtn_Hotel.MouseLeave += new System.EventHandler(this.button2_MouseLeave);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(260, 257);
            this.panel2.TabIndex = 30;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.label2.Location = new System.Drawing.Point(30, 186);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(203, 28);
            this.label2.TabIndex = 29;
            this.label2.Text = "Admin Dashboard";
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel3.Location = new System.Drawing.Point(88, 45);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(87, 72);
            this.panel3.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.label3.Location = new System.Drawing.Point(27, 122);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(247, 45);
            this.label3.TabIndex = 18;
            this.label3.Text = "We Travelers";
            // 
            // rbtnTrips
            // 
            this.rbtnTrips.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rbtnTrips.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtnTrips.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.rbtnTrips.BorderColor = System.Drawing.Color.Transparent;
            this.rbtnTrips.BorderRadius = 7;
            this.rbtnTrips.BorderSize = 0;
            this.rbtnTrips.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtnTrips.FlatAppearance.BorderSize = 0;
            this.rbtnTrips.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnTrips.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnTrips.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.rbtnTrips.Location = new System.Drawing.Point(20, 266);
            this.rbtnTrips.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbtnTrips.Name = "rbtnTrips";
            this.rbtnTrips.Size = new System.Drawing.Size(220, 46);
            this.rbtnTrips.TabIndex = 0;
            this.rbtnTrips.Text = "Trips Packages";
            this.rbtnTrips.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtnTrips.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(1)))));
            this.rbtnTrips.UseVisualStyleBackColor = false;
            this.rbtnTrips.Click += new System.EventHandler(this.roundButton1_Click);
            this.rbtnTrips.MouseEnter += new System.EventHandler(this.rbtnTrips_MouseEnter);
            this.rbtnTrips.MouseLeave += new System.EventHandler(this.rbtnTrips_MouseLeave);
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 10;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // pnlContainer
            // 
            this.pnlContainer.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlContainer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlContainer.BackgroundImage")));
            this.pnlContainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContainer.Location = new System.Drawing.Point(260, 0);
            this.pnlContainer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new System.Drawing.Size(1068, 969);
            this.pnlContainer.TabIndex = 1;
            this.pnlContainer.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlContainer_Paint);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.ClientSize = new System.Drawing.Size(1328, 969);
            this.Controls.Add(this.pnlContainer);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Panel pnlContainer;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private Properties.RoundButton rbtn_Hotel;
        private Properties.RoundButton rbtnTrips;
        private Properties.RoundButton rbtn_Rooms;
        private Properties.RoundButton rbtn_Payments;
        private Properties.RoundButton rbtn_Vehicles;
        private Properties.RoundButton rbtn_Exit;
        private Properties.RoundButton rbtn_Reports;
        private System.Windows.Forms.Panel panel3;
        private Properties.RoundButton roundButton1;
        private Properties.RoundButton roundButton2;
    }
}