﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Travel_Agency_Management_System.Forms.Admin
{
    public partial class View_of_Hotel_and_Hotel_Rooms : UserControl
    {
        private static View_of_Hotel_and_Hotel_Rooms _instance;
        public static View_of_Hotel_and_Hotel_Rooms Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new View_of_Hotel_and_Hotel_Rooms();
                }
                return _instance;
            }
        }
        public View_of_Hotel_and_Hotel_Rooms()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void View_of_Hotel_and_Hotel_Rooms_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            using (con)
            {

                SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM [Hotels and Rooms Views]", con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
        }
    }
}
