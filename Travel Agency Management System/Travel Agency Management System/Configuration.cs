﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Agency_Management_System
{



    class Configuration
    {
        //String ConnectionStr = @"Data Source=DESKTOP-TRNOSKU\MSSQLSERVER01;Initial Catalog=TAM_CHECK;Integrated Security=True";
        String ConnectionStr = @"Data Source=(local);Initial Catalog=TAM_CHECK;Integrated Security=True";
        SqlConnection con;
        private static Configuration _instance;
        public static Configuration getInstance()
        {
            if (_instance == null)
                _instance = new Configuration();
            return _instance;
        }
        private Configuration()
        {
            con = new SqlConnection(ConnectionStr);
            con.Open();
        }
        public SqlConnection getConnection()
        {
            return con;
        }
    }
}

